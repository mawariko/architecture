# Wakepark Application Architecture
For this project I would suggest **microservice** architecture. It is scalable, expandable, helps isolate mistakes (should they happen) and experiment with different technologies. On top of that it is considered to be better suited for modern web development.

When it comes to deciding what microservices to create and how they should communicate with the client and among themselves, the best choice seems to be the implementation of principles of Domain Driven Design.  

The core idea of this design pattern is to focus on the business and its logic (domain). Once established, we should proceed with further sectioning into subdomains looking for the *just-the-right-size* match to turn it into a microservice.

The size matters because we are looking for the balance between a service which is small enough to be independent, while at the same time trying to avoid *too-micro* services with very tight communication. Those should be combined into a bigger service with some internal dependencies, but improved productivity for the benefit of the whole app. 

Should a microservice need a place for storing data, it gets a database of its own, which has only relevant to the service information.

Mircoservices communicate with each other using REST API and message queues (RabbitMQ). Data retrieval is performed though REST API, while most of the updates are done via emitting events, which decouples application logic and allows more than one microservice respond to the same event.

After analyzing the business (wakeboarding) and the client (ambitious, wants to expand), I have come up with the following mircroservices:
* Gateway API. The name speaks for itself, it is the entry point for SPA and Mobile apps

* Authentification. Once the client enters the app, he/she has to go through the authentication process and receive a token, which willc contain user's id and be used as a 'pass' for  interactions with other services.

* RabbitMQ. This is message broker that will facilitate fast communication between the services and help avoid overloading of the server.

* Payments. As soon as the client has made a purchase (which might be a product or service requested from one of the microservices of our application), a message gets send to Payments (via RabbitMQ). There it is either confirmed or declined, based on the current user's amount of units and debt, after it is sent back via RabbitMQ. (Since most of the services' communication is happening via RabbitMQ, I will not specify it in the future. In case of confusion, there is a visual representation of this architecture at the end of this file and in the images folder)

* Statistics. Gathers data for analysis, e.g. what is the busiest month? what is the most popular drink? when do we have most visitors, etc

* Rewards & Complaints. This service works with both clients and employees. High-achievers and loyal clients should get some benefits and motivate others. As well as complaints from both sides should be takes into consideration. This service works closely with Statistics.

* Blog&Marketing. Gets data from Statistics and suggests best offers.

* Users. Keeps track of all the registered users and that part of their data, which is relevant for all the services (id, name, etc)

* Staff. Manages current employees.

* Equipment sale. Price, availability, new products.

* Equipment rent. Price, availability

* Restaurant. Since the client is going to expand, only a bar with snacks will not suffice. Hence the Restaurant service, which serves the current bar and is ready for future growth. 

* SPA. Although the client only offers massage now, I think we should make this service future-proof and allow for more spa-related services. 

* Beach. This service is basically responsible for keeping track of current visitors.

* Wakeboarding. Allows to see available time slots and tracks, book a track and an instructor

* File Server. Is used for storing and serving invoices, media files, menu pdfs, etc

* Notifications. Implements push notifications for mobile apps and web sockets for SPA

## Visual Representation of Architecture

![chart](https://i.ibb.co/fvSqCbq/Mircoservices.png)

