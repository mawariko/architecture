Contains source code, database migrations and Dockerfile for Blog & Marketing microservice. It Exposes REST API for managing articles and emits events reflecting changes in the repository. It has following entities:

* category

* post

* special offer

* user profile