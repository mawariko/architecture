Contains source code, database migrations and Dockerfile for Beach microservice. It exposes REST API for tracking beach usage. Uses following entities:

* beach

* entrance

* user profile

Beach entity represents one of the beaches of the wakepark. When entrance is created, a PurchaseCreated event is emitted. When PurchaseApproved event is obtained, the status of the entrance changes to PAID