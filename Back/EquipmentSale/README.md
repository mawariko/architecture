Contains source code, database migrations and Dockerfile for Equipment Sale microservice. It Exposes REST API for ordering and purchasing equipment. It uses following entities:

* category

* product

* order

* user profile

When order is created, a PurchaseCreated event is emitted. When PurchaseApproved event is obtained, the status of the order changes to PAID

