Contains source code, database migrations and Dockerfile for Statistics microservice. It collects statistical information coming from other services, it is subscribed to. It also Exposes REST API for making use of this information. It uses following entities:

* payment

* replenishment

* session

* feedback

* booking

* purchase

* user profile