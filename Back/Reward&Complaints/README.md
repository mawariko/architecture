Contains source code, database migrations and Dockerfile for Reward&Complaints microservice. It Exposes REST API for managing complaints and reward system, both for employees and clients. Uses following entities:

* complaint

* reward

