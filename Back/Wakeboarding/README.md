Contains source code, database migrations and Dockerfile for Wakeboarding microservice. It Exposes REST API for tracking track usage and booking, booking lessons, coaches, video and photo sessions. Uses following entities:

* track

* session

* subscription

* filming (includes video and photo sessions)

* video

* photo

* user profile


When session or filming is created, a PurchaseCreated event is emitted. When PurchaseApproved event is obtained, the status of the session/filming changes to PAID