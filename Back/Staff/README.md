Contains source code, database migrations and Dockerfile for Staff microservice. It Exposes REST API for managing employees and emits events reflecting changes in the repository. Uses following entities:

* user profile

* work shift