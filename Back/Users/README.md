Contains source code, database migrations and Dockerfile for Users microservice. It Exposes REST API for managing and persisting users and emits events reflecting changes in the repository. It uses only one entity:

* User

Currently there are four roles, but nothing stops our client from adding more
