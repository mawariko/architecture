Contains source code, database migrations and Dockerfile for Equipment Rent microservice. It Exposes REST API for booking, renting and managing the list of equipment. It uses following entities:

* category

* product

* booking

* session

* user profile

When session is created, a PurchaseCreated event is emitted. When PurchaseApproved event is obtained, the status of the session changes to PAID

