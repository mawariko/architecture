Contains source code, database migrations and Dockerfile for SPA microservice. Application exposes REST API for managing spa-services and booking. It uses following entities:

* session 

* booking

* user profile

When session is created, a PurchaseCreated event is emitted. When PurchaseApproved event is obtained, the status of the order changes to PAID