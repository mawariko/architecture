Contains source code, database migrations and Dockerfile for Payments microservice. It handles PurchaseCreate events and validates them.
It manages and persists transactions and exposes via REST API transaction history. This service is also responsible for replenishments. It uses the following entities:

* payment

* promo code

* payment system

* user profile