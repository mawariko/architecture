Contains source code, database migrations and Dockerfile for Restaurant microservice. Application exposes REST API for managing menu and stock items and creating orders. It uses following entities:

* category (category of the menu item, e.g. dish, drink, etc)

* menu item

* menu

* order (order is aggregation of a menu item and user's id and additional information)

* stock item

* supplier

* user profile (contains domain specific info about the user: preferences, allergies, etc)

When an order is created, a PurchaseCreated event is emitted. When PurchaseApproved event is obtained, the status of the order changes to PAID